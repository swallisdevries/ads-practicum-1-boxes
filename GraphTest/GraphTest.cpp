#include <cassert>
#include <iostream>
#include <vector>
#include <limits>
#include <algorithm> //sort

using namespace std;

/**
 * Pre-allocated linear queue
 */
template<class T>
class LinQueue
{
	vector<T> q_;
	decltype(q_.begin()) read_, write_;

public:
	LinQueue(const size_t size) : q_(size)
	{
		reset();
	}

	T& front()
	{
		assert(read_ < write_);
		return *read_;
	}

	void pop()
	{
		assert(read_ < write_);
		++read_;
	}

	void push(const T &val)
	{
		assert(write_ != q_.end());
		*write_ = val;
		++write_;
	}

	bool end() const
	{
		return read_ == write_;
	}

	void reset()
	{
		read_ = q_.begin();
		write_ = q_.begin();
	}
};



/**
 * Graph corresponding to the algorithm of https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm#Pseudocode
 */
class Graph
{
public:
	/**
	 * Vertex in V or nil
	 */
	struct VNilVertex {};

	/**
	 * Vertex in U or nil
	 */
	struct UNilVertex : VNilVertex
	{
		//Distance from source
		unsigned distance;
		//Infinite distance
		static const auto inf = numeric_limits<decltype(distance)>::max();
	};

	struct VVertex;

	/**
	 * Vertex in U
	 */
	struct UVertex : UNilVertex
	{
		vector<VVertex*> adjacent;
		VNilVertex *paired;
	};

	/**
	 * Vertex in V
	 */
	struct VVertex : VNilVertex
	{
		UNilVertex *paired;
	};

private:
	UNilVertex nil_;
	vector<UVertex> u_;
	vector<VVertex> v_;

public:
	/**
	 * \param uCount Number of matching (U) vertices
	 * \param vCount Number of matched (V) vertices
	 * \param reserveAdjacent The size to reserve for adjacent vertex vectors (or 0 to not call reserve)
	 */
	Graph(const size_t uCount, const size_t vCount, const unsigned reserveAdjacent = 0)
		: u_(uCount), v_(vCount)
	{
		if (reserveAdjacent)
			for (auto &u : u_) u.adjacent.reserve(reserveAdjacent);
	}

	UNilVertex& getNil()
	{
		return nil_;
	}

	/**
	 * Get matching (U) vertex at index
	 */
	UVertex& getU(const size_t index)
	{
		return u_[index];
	}

	/**
	 * Get matched (V) vertex at index
	 */
	VVertex& getV(const size_t index)
	{
		return v_[index];
	}

	/**
	 * Perform a breadth first search as defined in the algorithm
	 */
	bool bfs()
	{
		LinQueue<UNilVertex*> q(u_.size() + 1);

		for (auto &u : u_)
			if (u.paired == &nil_)
			{
				u.distance = 0;
				q.push(&u);
			}
			else u.distance = UNilVertex::inf;

		nil_.distance = UNilVertex::inf;

		while (!q.end())
		{
			const auto u = q.front();
			q.pop();
			if (u->distance < nil_.distance)
			{
				auto u2 = static_cast<UVertex*>(u);
				for (const auto v : u2->adjacent)
					if (v->paired->distance == UNilVertex::inf)
					{
						v->paired->distance = u->distance + 1;
						q.push(v->paired);
					}
			}
		}
		q.reset();

		return nil_.distance != UNilVertex::inf;
	}

	/**
	 * Perform a depth first search as defined in the algorithm
	 */
	bool dfs(UNilVertex * const u0)
	{
		if (u0 == &nil_) return true;

		const auto u = static_cast<UVertex*>(u0);
		for (auto v : u->adjacent)
			if (v->paired->distance == u->distance + 1
				&& dfs(v->paired))
			{
				v->paired = u;
				u->paired = v;
				return true;
			}
		u->distance = UNilVertex::inf;
		return false;
	}

	/**
	 * Perform Hopcroft-Karp on the graph (the algorithm running time is O(#edges * sqrt(#vertices)))
	 * \return The maximum matching value
	 */
	auto hopcroftKarp()
	{
		for (auto &u : u_)
		{
			u.paired = &nil_;
			u.adjacent.shrink_to_fit();
		}
		for (auto &v : v_) v.paired = &nil_;

		/*//Print average number of matchings per box:
		double matchings = 0;
		for (auto &u : u_)
			matchings += u.adjacent.size();
		cout << matchings / u_.size() << endl;*/

		unsigned matching = 0;
		while (bfs())
			for (auto &u : u_)
				if (u.paired == &nil_ && dfs(&u))
					++matching;

		return matching;
	}
};



//Use box volume for optimization?
#define SORT


struct Box
{
	float a, b, c
#ifdef SORT
	      , volume
#endif
	;

	Box() {}

	Box(const decltype(a) a, const decltype(b) b, const decltype(c) c)
		: a(a), b(b), c(c)
	{
#ifdef SORT
		updateVolume();
#endif
	}

#ifdef SORT
	void updateVolume()
	{
		volume = a * b * c;
	}
#endif

	bool fitsIn(const Box &o) const
	{
#ifdef SORT
		if (volume > o.volume) return false;
#endif
		return a < o.a && b < o.b && c < o.c
			|| a < o.a && c < o.b && b < o.c
			|| b < o.a && a < o.b && c < o.c
			|| b < o.a && c < o.b && a < o.c
			|| c < o.a && a < o.b && b < o.c
			|| c < o.a && b < o.b && a < o.c;
	}
};

auto& operator>>(istream &in, Box &box)
{
	in >> box.a >> box.b >> box.c;
#ifdef SORT
	box.updateVolume();
#endif
	return in;
}



//Uncomment to generate random boxes and display duration
//#define BENCHMARK 5000 //number of boxes

#ifdef BENCHMARK
#include <random>
#include <chrono>
#endif


int main()
{
	//Range [1, 5000]
	unsigned boxCount;
	vector<Box> boxes;

	//Fill boxes
#ifndef BENCHMARK
	cin >> boxCount;

	boxes.reserve(boxCount);

	//O(boxCount)
	for (unsigned index = 0; index < boxCount; ++index)
	{
		Box box;
		cin >> box;
		boxes.push_back(box);
	}
#else
	boxCount = BENCHMARK;
	boxes.reserve(boxCount);
	{
		default_random_engine random; //default constant seed
		uniform_real_distribution<decltype(Box::a)> dist(.501f); //cannot be const in every implementation

		for (unsigned index = 0; index < boxCount; ++index)
			boxes.emplace_back(dist(random), dist(random), dist(random));
	}
	const auto start = chrono::high_resolution_clock::now();
#endif

#ifdef SORT
	//eg. O(boxCount * log2(boxCount))
	sort(boxes.begin(), boxes.end(), [](auto &a, auto &b) { return a.volume < b.volume; });
#endif

	//Allocate graph
	Graph graph(boxCount, boxCount, boxCount / 4 * 3);

	//Compose graph
	//O(boxCount^2)
	for (unsigned innerIndex = 0; innerIndex < boxCount; ++innerIndex)
		for (unsigned outerIndex = 0; outerIndex < boxCount; ++outerIndex)
			if (boxes[innerIndex].fitsIn(boxes[outerIndex]))
				graph.getU(innerIndex).adjacent.push_back(&graph.getV(outerIndex));

	boxes.clear();

#ifdef BENCHMARK
	{
		const auto end = chrono::high_resolution_clock::now();
		cout << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms: built graph" << endl;
	}
#endif

	//Determine maximum matching/nesting
	//O(boxCount^2 * sqrt(2*boxCount)) = O(boxCount^2.5)
	const auto maxNestings = graph.hopcroftKarp();

#ifdef BENCHMARK
	{
		const auto end = chrono::high_resolution_clock::now();
		cout << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms tot: finished" << endl;
	}
#endif

	//Display result (minimum number of visible boxes)
	cout << boxCount - maxNestings << endl;
}
